function find(ele, cb) {  
      for(var i=0;i<ele.length;i++){
          if(ele[i]!=undefined){
              if(cb(ele[i])){
                  return ele[i];
              }
          }
      }
      return undefined;
  }

  module.exports = find;