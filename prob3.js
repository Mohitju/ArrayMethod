function reduce(ele, cb, startingValue) {
    
    var i=0;
    if(startingValue == undefined ){
        startingValue =ele[0];
        i++;
    }
    for(;i<ele.length;i++){
        if(ele[i]!=undefined){
            startingValue=cb(startingValue,ele[i]);
        }
    }
    return startingValue;
}

module.exports = reduce;