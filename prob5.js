function filter(ele, cb) {
    var arr = [];
    for(var i=0;i<ele.length;i++){
        if(ele[i]!=undefined){
            if(cb(ele[i])){
                arr.push(ele[i]);
            }
        }
    }
    return arr;
}

module.exports = filter;