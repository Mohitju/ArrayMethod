function flat(array,cb){
    for(let i=0;i < array.length; i++){
        let element = array[i];
        if(Array.isArray(element)){
            flat(element,cb);
        }
        else{
            cb(element);
        }
    }
}
module.exports = flat;