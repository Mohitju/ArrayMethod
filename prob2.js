function map(ele , cb){
    var arr = [];
    for(var i=0; i<ele.length;i++){
        if(ele[i] != undefined ){
            arr.push(cb(ele[i],i,ele));
        }
    }
    return arr;
}

module.exports =  map;